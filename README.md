Allgemeines
===========

Die Kalenderdaten werden aus drei Kategorien von Scripten erstellt. Diese werden jeweils mit tabulator-separierten Textdateien gefüttert. Dabei darf das Tabulator-Zeichen nicht durch Leerzeichen ersetzt werden!

* *_data_source.py beinhalten Texte, welche in die Kalender-Zellen eingetragen werden.
* * birthday_data_source.py: geburtstage.utf8.txt Das Format ist: %Zellentxt%[TAB]%Datum%.
* * holidays_data_source.py: DE_holidays.txt Das Format ist etwas komplexer.
* *_bg_create.py legt fest, welche Zeiträume mit einem farbigen Hintergrund markiert werden. Wenn sich zwei Zeiträume überschneiden, wird die Tageszelle aus mehreren Rechtecken zusammengebaut.
* * ferien_nrw_bg_create.py: ferien_nrw %JAHR%.txt Das Format ist %Zeitraumbeschreibung%[TAB]%Datum_Beginn% - %Datum_Ende%.
* * ferien_rlp_bg_create.py: ferien_rlp_bg_create %JAHR%.txt Das Format ist %Zeitraumbeschreibung%[TAB]%Datum_Beginn% - %Datum_Ende%
* *_decorator.py beschreibt, welche Zeiträume mit einem farbigen Strich gekennzeichnet werden. Es wird das selbe Format benutzt, wie
* * ramadan_decorator.py: ramadan%JAHR%.txt Das Format ist %Zeitraumbeschreibung%[TAB]%Datum_Beginn% - %Datum_Ende%.
