#!/usr/bin/env python
# -*- coding: utf-8 -*-

mod_path = "/home/andreas/Schreibtisch/Kalender Python-Scripte Scribus/"
year = 2023


# how much of the width shall be used for the week-number?
week_no_ratio = (1.0/4.0)

# width in mm, pos in mm
decorator_config = dict()
decorator_config["ramadan"] = {"width" : 1, "pos" : -1}

def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
