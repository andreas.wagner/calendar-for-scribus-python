#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import datetime

data = dict()

def init(year):
	data_file = open("ferien_rlp " + str(year) + ".txt")
	rows = data_file.readlines()
	for row in rows:
		if row != "\n":
			cols = row.split("\t")
			name = cols[0]
			begin_end = cols[1].strip().split(" - ")
			tmp = begin_end[0].split(".")
			begin = datetime.date(int(tmp[2]), int(tmp[1]), int(tmp[0]))
			tmp = begin_end[1].split(".")
			end = datetime.date(int(tmp[2]), int(tmp[1]), int(tmp[0]))
			data[begin] = name + " Anfang"
			data[end] = name + " Ende"
	return
	
def get_data_for_day(day):
	if day in data:
		return data[day]
	else:
		return None

def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
