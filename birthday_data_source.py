#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import scribus

data = dict()

def init(year):
	data_file = open("geburtstage.utf8.txt", "r")
	rows = data_file.readlines()
	for row in rows:
		if row != "\n":
			cols = row.split("\t")
			name = cols[0]
			day = cols[1].split(".")
			if datetime.date(year, int(day[1]), int(day[0])) in data:
				data[datetime.date(year, int(day[1]), int(day[0]))] = data[datetime.date(year, int(day[1]), int(day[0]))] + "\n" + name
			else:
				data[datetime.date(year, int(day[1]), int(day[0]))] = name
	return

def get_data_for_day(day):
	if day in data:
		return data[day]
	else:
		return None

def main(args):
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
