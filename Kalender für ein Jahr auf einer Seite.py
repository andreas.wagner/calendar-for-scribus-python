#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import calendar
import locale
import importlib
import glob
import my_config
import scribus
import os


mod_path = my_config.mod_path
year = my_config.year

linewidth = 0.25



cal = calendar.Calendar()

def putText(x, y, w, h, text, style):
	handle = scribus.createText(x, y, w, h)
	scribus.setText(text, handle)
	scribus.selectText(0, len(text), handle)
	scribus.setStyle(style, handle)
	return handle

def putDay(x, y, w, h, day):
	for deco in decorators.keys():
		if decorators[deco].get_data_for_day(day) == 1:
			rect = scribus.createRect(
				x + my_config.decorator_config[deco]["pos"],
				y,
				my_config.decorator_config[deco]["width"],
				h)
			scribus.setFillColor(deco+"_deco", rect)
			scribus.setLineColor(deco+"_deco", rect)
	
	bg_count = 0
	for bg in bg_creators.keys():
		if bg_creators[bg].get_data_for_day(day) == 1:
			bg_count = bg_count + 1
	
	if bg_count > 0:
		bg_iter = 0
		for bg in bg_creators.keys():
			if bg_creators[bg].get_data_for_day(day) == 1:
				rect = scribus.createRect(
					x+(w/bg_count)*bg_iter +linewidth,
					y +linewidth,
					w/bg_count-linewidth*2,
					h-linewidth*2)
				scribus.setFillColor(bg+"_bg", rect)
				scribus.setLineColor(bg+"_line", rect)
				bg_iter = bg_iter+1
	else:
		rect = scribus.createRect(
			x+linewidth,
			y+linewidth,
			w-linewidth*2,
			h-linewidth*2)
		if day.weekday() == 5:
			scribus.setFillColor("bg_saturday", rect)
			scribus.setLineColor("line_saturday", rect)
		else:
			if day.weekday() == 6:
				scribus.setFillColor("bg_sunday", rect)
				scribus.setLineColor("line_sunday", rect)
			else:
				scribus.setFillColor("bg", rect)
				scribus.setLineColor("line", rect)

	putText(
		x+linewidth, y+linewidth,
		w * my_config.week_no_ratio -linewidth*2,
		h -linewidth*2,
		day.strftime("%d %a"),
		"day")
	day_handle = putText(
		x + w * my_config.week_no_ratio + linewidth, y +linewidth,
		w * (1-my_config.week_no_ratio) -2*linewidth,
		h -2*linewidth,
		"",
		"day")
	my_map = dict()
	text = ""
	linecount = 0
	for key in data_sources.keys():
		to_append = data_sources[key].get_data_for_day(day)
		
		if to_append is not None:
			if text != "":
				tmp1 = len(text) + linecount
				text = text + "\n" + to_append
				tmp2 = len(to_append)
			else:
				text = to_append
				tmp1 = 0
				tmp2 = len(to_append)
			my_map[key] = (tmp1, tmp2)
			linecount = linecount + 1;

	for key in data_sources.keys():
			if(text != ""):
				scribus.setText(text, day_handle)
				for key in my_map.keys():
					begin = my_map[key][0]+1
					length = my_map[key][1]-1
					scribus.selectText(begin, length, day_handle)
					scribus.setStyle("ds_" + key, day_handle)

def putMonth(x1, y1, w, h, month_no):
	month = month_no
	for day in cal.itermonthdates(year, month+1):
		if day.month != month+1:
			continue
		putDay(
			x1,
			y1+h/31.0*day.day,
			w,
			h/31.0,
			day
			)

decorators = dict()
bg_creators = dict()
data_sources = dict()

def main(args):
	# the files named mod_prefix + "*.py" will be imported and
	# from each file, init(year) will be called. For each day,
	# get_data_for_day(day) will be called.
	mod_suffix = "_data_source"
	decorator_suffix = "_decorator"
	bg_create_suffix = "_bg_create"



	# import and init all data-sources
	path_bak = os.getcwd()
	os.chdir(mod_path)

	files = glob.glob("*" + mod_suffix + ".py")
	for my_file in files:
		py_index = my_file.rindex(mod_suffix)
		my_file = my_file[0:py_index] + mod_suffix
		mod_name = my_file[0:py_index]
		data_sources[mod_name] = importlib.import_module(my_file)
		data_sources[mod_name].init(year)

	files = glob.glob("*" + decorator_suffix + ".py")
	for my_file in files:
		py_index = my_file.rindex(decorator_suffix)
		my_file = my_file[0:py_index] + decorator_suffix
		mod_name = my_file[0:py_index]
		if mod_name in my_config.decorator_config.keys(): # only load configured decorators
			decorators[mod_name] = importlib.import_module(my_file)
			decorators[mod_name].init(year)

	files = glob.glob("*" + bg_create_suffix + ".py")
	for my_file in files:
		py_index = my_file.rindex(bg_create_suffix)
		my_file = my_file[0:py_index] + bg_create_suffix
		mod_name = my_file[0:py_index]
		bg_creators[mod_name] = importlib.import_module(my_file)
		bg_creators[mod_name].init(year)

	os.chdir(path_bak)

	# make sure we have an open document
	if not scribus.haveDoc():
		return 1

	# use all data-source-names to create character styles
	for src_name in data_sources.keys():
		scribus.createCharStyle(
			name = "ds_c_" + src_name,
			font="FreeSans Regular",
			fontsize=12.0,
			fillcolor="Black"
			)
		scribus.createParagraphStyle(
			name = "ds_" + src_name,
			charstyle= "ds_c_" + src_name
			)
	for src_name in decorators.keys():
		scribus.defineColorCMYK(src_name + "_deco", 255, 100, 0, 0)
	for src_name in bg_creators.keys():
		scribus.defineColorCMYK(src_name + "_bg", 255, 100, 0, 0)
	for src_name in bg_creators.keys():
		scribus.defineColorCMYK(src_name + "_line", 255, 100, 0, 0)

	scribus.defineColorCMYK("bg", 255, 100, 0, 0)
	scribus.defineColorCMYK("bg_saturday", 255, 100, 0, 0)
	scribus.defineColorCMYK("bg_sunday", 255, 100, 0, 0)
	scribus.defineColorCMYK("line", 255, 100, 0, 0)
	scribus.defineColorCMYK("line_saturday", 255, 100, 0, 0)
	scribus.defineColorCMYK("line_sunday", 255, 100, 0, 0)
	scribus.defineColorCMYK("month_bg", 255, 100, 0, 0)
	scribus.defineColorCMYK("month_line", 255, 100, 0, 0)
	scribus.createParagraphStyle(name = "day")
	scribus.createParagraphStyle(name = "day_name")
	scribus.createParagraphStyle(name = "week_number")
	scribus.createParagraphStyle(name = "month")

	scribus.createLayer("Calendar")


#	scribus.newPage(-1, scribus.masterPageNames()[0])
	(width, height) = scribus.getPageSize()
	(topMargin, leftMargin, rightMargin, bottomMargin) = scribus.getPageMargins()
	day_width = (width - (leftMargin + rightMargin)) / 12.0
	day_height = ((height - (topMargin + bottomMargin)) / 32.0)

# create the calendar
	scribus.progressReset()
	scribus.progressTotal(12)
	i = 0
	for month in range(0, 12):
		rect = scribus.createRect(
			leftMargin + month/12 * (width-(leftMargin+rightMargin))+0.5,
			topMargin+0.5,
			(width - (leftMargin + rightMargin))/12.0 -1.0,
			(height - (topMargin + bottomMargin) )/32.0 -1.0
			)
		scribus.setFillColor("month_bg", rect)
		scribus.setLineColor("month_line", rect)
		putText(
			leftMargin + month/12 * (width-(leftMargin+rightMargin))+0.5,
			topMargin+0.5,
			(width - (leftMargin + rightMargin))/12.0 -1.0,
			(height - (topMargin + bottomMargin) )/32.0 -1.0,
			calendar.month_name[month+1],
			"month"
			)
		putMonth(
			leftMargin + month/12.0 * (width - (leftMargin+rightMargin)),
			topMargin,
			(width - (leftMargin+rightMargin))/12,
			(height - (topMargin+bottomMargin)) - (height - (topMargin + bottomMargin) )/32.0,
			month
			)
		scribus.progressSet(month)
	return 0

if __name__ == '__main__':
    import sys
    locale.setlocale(locale.LC_ALL, '')
    sys.exit(main(sys.argv))
