#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import scribus

data = dict()

def init(year):
	a = year % 19
	b = year // 100
	c = year % 100
	d = (19 * a + b - b // 4 - ((b - (b + 8) // 25 + 1) // 3) + 15) % 30
	e = (32 + 2 * (b % 4) + 2 * (c // 4) - d - (c % 4)) % 7
	f = d + e - 7 * ((a + 11 * d + 22 * e) // 451) + 114	
	easter = datetime.date(int(year), int(f//31), int(f % 31 + 1))
	
	data_file = open("DE_holidays.txt", "r")
	rows = data_file.readlines()
	for row in rows:
		if row != "\n":
			cols = row.split(",")
			variable_or_fixed = cols[0]
			month_or_calendar = cols[1]
			offset_or_day = cols[2]
			name = cols[3]
			if variable_or_fixed == "variable":
				if month_or_calendar == "easter":
					data[easter + datetime.timedelta(days = int(offset_or_day))] = name
				else:
					return
			else:
				data[datetime.date(year, int(month_or_calendar), int(offset_or_day))] = name
	return

def get_data_for_day(day):
	if day in data:
		return data[day]
	else:
		return None

def main(args):
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
